package com.gmail.Test;

import com.gmail.DriverManager;
import com.gmail.businessObject.GmailLogin;
import com.gmail.businessObject.GmailMoveMsgToImp;
import com.gmail.unit.User;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import static com.gmail.businessObject.GmailMoveMsgToImp.LOG;

public class GmailTest {


    @BeforeSuite(alwaysRun = true)
    public void configSuite() {


        LOG.info("Test started!");

    }


    @BeforeClass
    public void setSystem() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }



    @Test(dataProvider = "getUser")
    public void gmailTest(User user){
        WebDriver driver = DriverManager.getInstance().getDriver();
        driver.get("https://www.gmail.com");
        GmailLogin gmailLogin=new GmailLogin(driver);
        GmailMoveMsgToImp moveToImp=new GmailMoveMsgToImp(driver);
        gmailLogin.logIn(user.getUserName(),user.getPsw());
        moveToImp.moveToImpFolder();
        Assert.assertTrue(moveToImp.varifyMoveToImpFolder());
        moveToImp.deleteImpMessage();
        Assert.assertTrue(moveToImp.varifyDeleteMessage());
        driver.quit();
    }

    @DataProvider(parallel = true)
    public Object[][] getUser() throws FileNotFoundException {
        JsonElement jsonData = new JsonParser().parse(new FileReader("src/main/resources/user.json"));
        JsonElement userSet = jsonData.getAsJsonObject().get("userSet");
        List<User> testData = new Gson().fromJson(userSet, new TypeToken<List<User>>(){}.getType());
        Object[][] returnValue = new Object[testData.size()][1];
        int index = 0;
        for (Object[] each : returnValue) {
            each[0] = testData.get(index++);
        }
        return returnValue;
    }
}
