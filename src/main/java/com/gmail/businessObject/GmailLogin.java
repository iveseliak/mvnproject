package com.gmail.businessObject;

import com.gmail.pom.GmailPageObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;


public class GmailLogin {
    public static final Logger LOG = LogManager.getLogger(GmailLogin.class);

    private WebDriver driver;
    private GmailPageObject object;

    public GmailLogin(WebDriver driver){
        object = new GmailPageObject(driver);
        this.driver=driver;
    }

    public void logIn(String username, String pass)
    {
        object.getLoginInput().sendKeys(username);
        object.getNextButton().click();
        object.getPasswordInput().sendToHideField(driver, pass);
        object.getNextPassButton().click();
        LOG.info("LogIn pass successful");
    }
}
